FROM python:3.8

ENV DOCKERIZE_VERSION v0.6.1

WORKDIR /app

COPY . /app

RUN pip install --no-cache-dir -r requirements.txt
RUN wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz

CMD ["python", "main.py"]
